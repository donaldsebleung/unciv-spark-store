# unciv-spark-store

## 介绍

[Unciv](https://github.com/yairm210/Unciv) deb 包，供[星火应用商店](https://spark-app.store/)上架

## 使用说明

于仓库根目录执行以下命令：

```bash
$ ./build-deb.sh
```

## 许可证

[MIT](./LICENSE) （仅限本仓库文档，不包含 Unciv 上游代码）

Unciv 上游代码使用 [MPLv2](https://github.com/yairm210/Unciv/blob/master/LICENSE) 许可证
