#!/bin/bash -e

rm -rf ./dl-assets/
mkdir -p ./dl-assets/
pushd dl-assets/
wget https://github.com/yairm210/Unciv/releases/download/4.6.11-patch1/Unciv.jar
popd
dpkg-buildpackage -us -uc
lintian ../unciv_4.6.11~spark2_all.deb
